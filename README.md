# Natural Steganography in JPEG Domain with a Linear Development Pipeline

**Authors:** 

* Théo Taburet, CNRS, Ecole Centrale de Lille, CRIStAL Lab, 59651 Villeneuve d’Ascq Cedex, France, <theo.taburet@centralelille.fr>
* Patrick Bas, CNRS, Ecole Centrale de Lille, CRIStAL Lab, 59651 Villeneuve d’Ascq Cedex, France <patrick.bas@centralelille.fr>
* Wadih Sawaya, IMT Lille-Douai, Univ. Lille, CNRS, Centrale Lille, UMR 9189, France, <wadih.sawaya@imt-lille-douai.fr>
* Jessica Fridrich, Department of ECE, SUNY Binghamton, NY, USA, <fridrich@binghamton.edu>

**Paper:** <https://arxiv.org/abs/2001.02653>

## Motivations :

This python notebook is an additional material to the paper. It has been added **for the sake of reproducibility** of the results of the results.
