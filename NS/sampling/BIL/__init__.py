import numpy as np
import itertools
import scipy.linalg as LA
import rawpy
import os

from tqdm.notebook import tqdm
from scipy.special import erf
from NS.compute import BIL as Covariance
from ...tools import *

class Sampler():
    def __init__(self, cover_path, seed, strategy = "4-Lattices", a = 1.15, b = -1150.0):
        self.cover_path = cover_path
        self.raw_cover = rawpy.imread(cover_path) #Cover file
        if(strategy != "Pseudo-spatial"):
            self.cover_dct = raw_to_dct([self.raw_cover, os.path.basename(self.cover_path)])[:, :, 0] #Luminance only
        else:
            self.cover_spatial = raw_to_ycbcr([self.raw_cover, os.path.basename(self.cover_path)])[:, :, 0] #Luminance only
        self.cover = self.raw_cover.raw_image[:, :]
        self.seed = seed #Seed for the RNG
        self.strategy = strategy
        self.a = a
        self.b = b
        self.eps = 10**(-3)

    # Calcul de la covariance pour chaque photo-site
    def compute_PP_T(self, B, n_b):
        n_c = n_b*8+2
        B2 = B*self.a+self.b
        B2 = np.ravel(B2) 

        PP_T = np.zeros((n_c**2,n_c**2))

        B2 = B2*16
        PP_T[np.diag_indices(PP_T.shape[1])] = B2
        
        return PP_T

    def compute_cov(self, M, PP_T):
        _cov = np.dot(M, np.dot(PP_T,M.T))
        return _cov

    def process(self, cov, obs, i, j):
        #For overloading purposes
        return self

    def computeStego(self):
        if(self.strategy == "4-Lattices"):
            self.Embedding_4_lattices()
        elif(self.strategy == "Intra-only"):
            self.Embedding_intra_only()
        elif(self.strategy == "Independant"):
            self.Embedding_independant()
        elif(self.strategy == "Pseudo"):
            self.Embedding_pseudo()
        elif(self.strategy == "Pseudo-spatial"):
            self.Embedding_pseudo()

        return self

    def Embedding_4_lattices(self):
        np.random.seed(self.seed) #Feed the RNG

        cover, dct_cover = self.cover, self.cover_dct
        h, w = self.cover_dct.shape
        dct_stego = np.zeros((h, w))

        #h, w = self.cover.shape
        #cover, dct_cover = self.cover, self.cover_dct
        #dct_stego = np.zeros((h, w))
        
        M_1 = Covariance.getPhotositesToDctMatrix(1, 'Y') #comp_M(1)
        M_3 = Covariance.getPhotositesToDctMatrix(3, 'Y')#comp_M(3)
        
        # Lattices loaders
        pbar1 = tqdm(total=(h//16-1)*(w//16-1), desc = "Lattices 1")
        pbar2 = tqdm(total=(h//16-2)*(w//16-2), desc = "Lattices 2")
        pbar3 = tqdm(total=(h//16-2)*(w//16-2), desc = "Lattices 3")
        pbar4 = tqdm(total=(h//16-2)*(w//16-2), desc = "Lattices 4")

        #Lattice 1
        for i, j in itertools.product(range((h//16-1)), range(w//16-1)):
            c = cover[i*16+7:i*16+17 , j*16+7:j*16+17]
            PP_T = self.compute_PP_T(c, 1)
            cov_L1 = self.compute_cov(M_1, PP_T)
            dct_stego[i*16+8:i*16+16 , j*16+8:j*16+16 ] = self.process(cov_L1+self.eps*np.eye(64), np.zeros(64), pos = {'i_left' : i*16+8, 'i_right' : i*16+16, 'j_left' : j*16+8, 'j_right' : j*16+16 })

            pbar1.update(1)
        
        #Lattice 2
        for i, j in itertools.product(range((h//16-2)), range(w//16-2)):          
            obs = np.concatenate(((dct_stego[i*16+8:i*16+16 , j*16+8:j*16+16 ]).ravel(),\
                                (dct_stego[i*16+8:i*16+16 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+8:j*16+16 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+16:j*16+24 ]).ravel()))
            
            c = cover[i*16+7:i*16+33 , j*16+7:j*16+33]
            PP_T = self.compute_PP_T(c, 3)
            cov_L4 = self.compute_cov(M_3, PP_T)            
            #cov_L2 = np.block([ [cov_L4[-4*64:, -4*64:], cov_L4[-4*64:, :64]],
            #                    [cov_L4[:64, -4*64:],    cov_L4[:64, :64]]])
            cov_L2 = cov_L4[:5*64,:5*64]
            cov_L2[:64,64:] = cov_L4[:64,5*64:]
            cov_L2[64:,:64] = cov_L4[5*64:,:64]
            cov_L2[64:,64:] = cov_L4[5*64:,5*64:]

            dct_stego[i*16+16:i*16+24 , j*16+16:j*16+24 ] = self.process(cov_L2+self.eps*np.eye(5*64), obs, pos = {'i_left' : i*16+16, 'i_right' : i*16+24, 'j_left' : j*16+16, 'j_right' : j*16+24 })
            pbar2.update(1)
            
        #Lattice 3 
        for i, j in itertools.product(range((h//16-2)), range(w//16-2)):
            obs = np.concatenate(((dct_stego[i*16+8:i*16+16 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+16:j*16+24 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+32:j*16+40 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+24:j*16+32 ]).ravel()))

            c = cover[i*16+7:i*16+33 , j*16+15:j*16+41 ]
            PP_T = self.compute_PP_T(c, 3)
            cov_L4 = self.compute_cov(M_3, PP_T)            
            cov_L3 = cov_L4[:5*64,:5*64]

            dct_stego[i*16+16:i*16+24 , j*16+24:j*16+32 ] = self.process(cov_L3+self.eps*np.eye(5*64), obs, pos = {'i_left' : i*16+16, 'i_right' : i*16+24, 'j_left' : j*16+24, 'j_right' : j*16+32 })
            pbar3.update(1)
            
        #Lattice 4
        for i, j in itertools.product(range((h//16-2)), range(w//16-2)):
            obs = np.concatenate(((dct_stego[i*16+16:i*16+24 , j*16+16:j*16+24 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+8:j*16+16 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+32:i*16+40 , j*16+16:j*16+24 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+8:j*16+16 ]).ravel(),\
                                (dct_stego[i*16+16:i*16+24 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+32:i*16+40 , j*16+8:j*16+16 ]).ravel(),\
                                (dct_stego[i*16+32:i*16+40 , j*16+24:j*16+32 ]).ravel(),\
                                (dct_stego[i*16+24:i*16+32 , j*16+16:j*16+24 ]).ravel()))

            c = cover[i*16+15:i*16+41 , j*16+7:j*16+33 ]
            PP_T = self.compute_PP_T(c, 3)
            cov_L4 = self.compute_cov(M_3,PP_T)            
            
            dct_stego[i*16+24:i*16+32 , j*16+16:j*16+24 ] = self.process(cov_L4+self.eps*np.eye(9*64), obs, pos = {'i_left' : i*16+24, 'i_right' : i*16+32, 'j_left' : j*16+16, 'j_right' : j*16+24 })
            pbar4.update()
        
        self.stego = dct_stego + dct_cover
        return self.stego

    def Embedding_pseudo(self):
        nb_bits = 14  
        #lvl_max = 2**nb_bits-1

        im_cover = self.raw_cover.raw_image
        raw_stego = self.raw_cover
        h, w = self.cover.shape

        im_stego = np.zeros((h, w))

        lvl_max = np.max(im_cover[:,:]) #2**nb_bits-1
        lvl_min = np.min(im_cover[:,:]) #2**nb_bits-1

        var_stego_signal = im_cover[:, :]*self.a+self.b
        var_stego_signal[var_stego_signal<0]=0
        sens_noise = np.random.randn(h, w)*np.sqrt(var_stego_signal)

        im_stego[:,:] = im_cover[:, :] + sens_noise

        # Wet pixels
        im_stego[im_stego>lvl_max]=lvl_max
        im_stego[im_stego<lvl_min]=lvl_min
        im_stego[im_cover==lvl_max]=lvl_max
        im_stego[im_cover==lvl_min]=lvl_min
                      
        raw_stego.raw_image[:, :] = im_stego[:, :]

        self.stego = raw_to_ycbcr([raw_stego, os.path.basename(self.cover_path)])[:, :, 0] #Luminance only
        return self.stego

    def Embedding_pseudo_spatial(self):
        nb_bits = 14  
        lvl_max = 2**nb_bits-1

        im_cover = self.raw_cover.raw_image
        raw_stego = self.raw_cover
        h, w = self.cover.shape

        im_stego = np.zeros((h, w))

        var_stego_signal = im_cover[:, :]*self.a+self.b
        var_stego_signal[var_stego_signal<0]=0
        sens_noise = np.random.randn(h, w)*np.sqrt(var_stego_signal)

        im_stego[:, :] = im_cover[:, :] + sens_noise

        # Wet pixels
        im_stego[im_stego>lvl_max]=lvl_max
        im_stego[im_stego<0]=0
        im_stego[im_cover==lvl_max]=lvl_max
        im_stego[im_cover==0]=0
  
        raw_stego.raw_image[:, :] = im_stego[:, :]
        self.stego = raw_to_ycbcr([raw_stego, os.path.basename(self.cover_path)])[:, :, 0] #Luminance only
        return self.stego

    def Embedding_intra_only(self):
        np.random.seed(self.seed) #Feed the RNG

        cover, dct_cover = self.cover, self.cover_dct
        h, w = self.cover_dct.shape
        dct_stego = np.zeros((h, w))
        
        M_1 = Covariance.getPhotositesToDctMatrix(1, 'Y')

        # Lattices loaders
        pbar1 = tqdm(total=(h//8-2)*(w//8-2), desc = "Intra-only")

        #Lattice 1
        for i, j in itertools.product(range(h//8-2), range(w//8-2)):
            c = cover[i*8+7:i*8+17 , j*8+7:j*8+17]
            PP_T = self.compute_PP_T(c, 1)
            cov_L1 = self.compute_cov(M_1, PP_T)
            dct_stego[i*8+8:i*8+16 , j*8+8:j*8+16 ] = self.process(cov_L1+self.eps*np.eye(64), np.zeros(64), pos = {'i_left' : i*8+8, 'i_right' : i*8+16, 'j_left' : j*8+8, 'j_right' : j*8+16 })
            pbar1.update(1)

        self.stego = dct_stego + dct_cover
        return self.stego

    def Embedding_independant(self):
        np.random.seed(self.seed) #Feed the RNG

        cover, dct_cover = self.cover, self.cover_dct
        h, w = self.cover_dct.shape
        dct_stego = np.zeros((h, w))

        M_1 = Covariance.getPhotositesToDctMatrix(1, 'Y')

        # Lattices loaders
        pbar1 = tqdm(total=(h//8-2)*(w//8-2), desc = "Independant")

        #Lattice 1
        for i, j in itertools.product(range(h//8-2), range(w//8-2)):
            c = cover[i*8+7:i*8+17 , j*8+7:j*8+17]
            PP_T = self.compute_PP_T(c, 1)
            cov_L1 = self.compute_cov(M_1, PP_T)
            dct_stego[i*8+8:i*8+16 , j*8+8:j*8+16 ] = self.process(np.eye(64)*cov_L1+self.eps*np.eye(64), np.zeros(64), pos = {'i_left' : i*8+8, 'i_right' : i*8+16, 'j_left' : j*8+8, 'j_right' : j*8+16 })
            pbar1.update(1)

        self.stego = dct_stego + dct_cover
        return self.stego

    def getStego(self, Qf = 100, crop_size = (0, 0)):
        # if(self.strategy == "Pseudo-spatial"):
        #     self.stego_quantized = (self.stego//256).astype(dtype=np.uint8)
        #     if(crop_size != (0, 0) and len(list(crop_size)) == 2):
        #         s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
        #         return self.stego_quantized, [self.stego_quantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
        #     else:
        #         return self.stego_quantized
        if(self.strategy == "Pseudo-spatial"): #16bits
            self.stego_unquantized = (self.stego).astype(dtype=np.uint16)
            if(crop_size != (0, 0) and len(list(crop_size)) == 2):
                s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
                return self.stego_unquantized, [self.stego_unquantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
            else:
                return self.stego_unquantized
        elif(self.strategy == "Pseudo"):
            Q_Y = np.load('Q_Y.npy')[Qf-1]
            self.stego_unquantized = compute_dct_domain(self.stego)
            self.stego_quantized = compute_jpeg_from_dct(self.stego_unquantized, Q_Y)#[8:-16,16:-8]

            if(crop_size != (0, 0) and len(list(crop_size)) == 2):
                crop_size = crop_size[0]-crop_size[0]%8, crop_size[1]-crop_size[1]%8
                s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
                return self.stego_unquantized, self.stego_quantized, [ self.stego_quantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
            else:
                return self.stego_unquantized, self.stego_quantized
        else:
            Q_Y = np.load('Q_Y.npy')[Qf-1]
            self.stego_quantized = None
            if(self.strategy == "4-Lattices"):
                self.stego_quantized = compute_jpeg_from_dct(self.stego, Q_Y)[16:-16, 16:-16]
            else:
                self.stego_quantized = compute_jpeg_from_dct(self.stego, Q_Y)[8:-8, 8:-8]

            if(crop_size != (0, 0) and len(list(crop_size)) == 2):
                crop_size = crop_size[0]-crop_size[0]%8, crop_size[1]-crop_size[1]%8
                s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
                return self.stego, self.stego_quantized, [ self.stego_quantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
            else:
                return self.stego, self.stego_quantized

    # def getCover(self, Qf = 100, crop_size = (0, 0)):
    #     if(self.strategy == "Pseudo-spatial"):
    #         cover = raw_to_ycbcr(self.raw_cover)[:, :, 0] #Luminance only

    #         if(crop_size != (0, 0) and len(list(crop_size)) == 2):
    #             s_i, s_j =  cover.shape[0]//crop_size[0],  cover.shape[1]//crop_size[1]
    #             return cover, [ cover[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
    #         else:
    #             return 
    #     else :
    #         Q_Y = np.load('Q_Y.npy')[Qf-1]

    #         cover = compute_jpeg_from_dct(raw_to_dct(self.raw_cover)[:, :, 0], Q_Y)[8:-16,16:-8]
    #         if(crop_size != (0, 0) and len(list(crop_size)) == 2):
    #             s_i, s_j =  cover.shape[0]//crop_size[0],  cover.shape[1]//crop_size[1]
    #             return cover, [ cover[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
    #         else:
    #             return cover

class MVG(Sampler):
    def __init__(self, cover_path, seed, strategy = "4-Lattices"):
        Sampler.__init__(self, cover_path, seed, strategy = strategy)

    def process(self, cov, obs, pos):
        cov_11 = cov[:64,:64]
        cov_12 = cov[:64,64:]
        cov_21 = cov[64:,:64]
        cov_22 = cov[64:,64:]
        
        obs = obs[:-64]
        
        reg_mat = np.dot(cov_12, np.linalg.pinv(cov_22))
        cov_schur = cov_11 - np.dot(reg_mat, cov_21)

        mean_vec = np.zeros(cov.shape[0])
        mean = mean_vec[:64] + np.dot(reg_mat, obs-mean_vec[64:])
        cov_schur = (cov_schur + cov_schur.T)/2#+ self.eps*np.eye(64)

        return np.random.multivariate_normal(mean, cov_schur).reshape( (8, 8) )

    # def getStego(self, Qf, crop_size = (0, 0)):
    #     Q_Y = np.load('Q_Y.npy')[Qf-1]

    #     self.stego_quantized = compute_jpeg_from_dct(self.stego, Q_Y)[8:-16,16:-8]
    #     if(crop_size != (0, 0) and len(list(crop_size)) == 2):
    #         s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
    #         return self.stego_quantized, [ self.stego_quantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
    #     else:
    #         return self.stego_quantized

class Cholesky(Sampler):
    # def __init__(self, cover, Q, K, seed, strategy = "4-Lattices"):
    #     Sampler.__init__(self, cover, Q, K, seed, strategy = strategy)
    #     self.wn = np.random.normal(0, 1, self.cover.shape) #White Noise
    
    def __init__(self, cover_path, seed, strategy = "4-Lattices"):
        Sampler.__init__(self, cover_path, seed, strategy = strategy)
        self.wn = np.random.normal(0, 1, self.cover.shape) #White Noise

    def process(self, cov, obs, pos):
        cov_11 = cov[:64,:64]
        cov_12 = cov[:64,64:]
        cov_21 = cov[64:,:64]
        cov_22 = cov[64:,64:]

        reg_mat = np.dot(cov_12, np.linalg.pinv(cov_22))
        cov_schur = cov_11 - np.dot(reg_mat, cov_21)#+ self.eps*np.eye(64)
        
        L = LA.cholesky(cov_schur, lower=True) #Cholesky decomposition
        
        if(obs.shape[0] > 64):
            return (np.dot(L, self.wn[pos['i_left']:pos['i_right'], pos['j_left']:pos['j_right']].ravel()) + np.dot(reg_mat, obs[:-64])).reshape((8, 8))
        else:
            return np.dot(L, self.wn[pos['i_left']:pos['i_right'], pos['j_left']:pos['j_right']].ravel()).reshape((8, 8))
    
    # def getStego(self, Qf, crop_size = (0, 0)):
    #     Q_Y = np.load('Q_Y.npy')[Qf-1]

    #     self.stego_quantized = compute_jpeg_from_dct(self.stego, Q_Y)[8:-16,16:-8]
    #     if(crop_size != (0, 0) and len(list(crop_size)) == 2):
    #         s_i, s_j =  self.stego.shape[0]//crop_size[0],  self.stego.shape[1]//crop_size[1]
    #         return self.stego_quantized, [ self.stego_quantized[crop_size[0]*i:crop_size[0]*(i+1), crop_size[1]*j:crop_size[1]*(j+1)] for i, j in itertools.product(range(s_i), range(s_j)) ]
    #     else:
    #         return self.stego_quantized

class RJ(Sampler):
    # def __init__(self, cover, Q, K, seed, strategy = "4-Lattices"):
    #     Sampler.__init__(self, cover, Q, K, seed, strategy = strategy)
    #     self.C, self.c = np.zeros((64, 1), dtype = np.int), np.zeros((64, 1), dtype = np.float)

    def __init__(self, cover_path, Qf, K, seed, strategy = "4-Lattices"):
        Sampler.__init__(self, cover_path, seed, strategy = strategy)
        self.C, self.c = np.zeros((64, 1), dtype = np.int), np.zeros((64, 1), dtype = np.float)
        self.K =  np.array(np.arange(-K, K+1)) #Alphabet
        self.Kn = 2*K+1 #Length of the alphabet
        self.Qf = Qf
        self.Q = np.load('Q_Y.npy')[Qf-1].ravel()

    def _findContinuousValue(self, m, s, uk, uk1):
        # Find continuous values
        while True:
            _c = np.random.normal(m, s)
            for i in range(self.Kn):
                if( uk[ i ] < _c <= uk1[ i ] ): #Check if u_{k} < c <u_{k+1}
                    return _c
        
    def generateSamples(self, i = 0):
        np.random.seed(None)
        
        if(i!=64):
            #PDF
            m, s = np.dot(self.L[i, :i], self.c[:i])[0], self.L[i, i] #Mean and Std
            
            #Shriking
            _m, _s = m/self.Q[i], s/self.Q[i]

            #Building UK+1, UK
            UK1, UK = 0.5+self.K+np.round(_m), -0.5+self.K+np.round(_m)
            UK[0], UK1[-1] = -np.inf, np.inf

            X = np.kron(_m, np.ones(self.Kn))
            M = np.sqrt(0.5)*np.reciprocal(np.kron(_s, np.ones(self.Kn, dtype=np.float)))
            
            P = 0.5*(erf(np.multiply(UK1-X, M))-erf(np.multiply(UK-X, M)))
            
            #Sample C1 using random choice
            self.C[i] = np.random.choice(self.K, 1, p=P)[0]

            #Sample c1 using rejection-sampling
            self.c[i] = self._findContinuousValue(m, s, UK, UK1)
            
            if(i!=63):
                self.generateSamples(i+1)
    
    def getSamples(self):
        self.generateSamples()
        return self.C, self.c
    
    def process(self, cov, obs, pos):
        cov_11 = cov[:64,:64]
        cov_12 = cov[:64,64:]
        cov_21 = cov[64:,:64]
        cov_22 = cov[64:,64:]

        reg_mat = np.dot(cov_12, np.linalg.pinv(cov_22))
        cov_schur = cov_11 - np.dot(reg_mat, cov_21)# + self.eps*np.eye(64)
        
        self.L = LA.cholesky(cov_schur, lower=True) #Cholesky decomposition
        
        return self.getSamples()[1].reshape((8, 8))
        
    def getSamples(self):
        self.generateSamples()
        return self.C, self.c