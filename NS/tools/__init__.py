import numpy as np
import os
import scipy
import rawpy
import skimage.io

from numpy import cos
from scipy.fftpack import dct, idct
from tqdm.notebook import tqdm


params = rawpy.Params(rawpy.DemosaicAlgorithm.LINEAR, half_size=False, four_color_rgb=False, use_camera_wb=False, use_auto_wb=False, user_wb=(1,1,1,1), output_color=rawpy.ColorSpace.raw, output_bps=16, user_flip=None, user_black=0, user_sat=None, no_auto_bright=True, auto_bright_thr=None, adjust_maximum_thr=0.0, bright=1.0, highlight_mode=rawpy.HighlightMode.Clip, exp_shift=None, exp_preserve_highlights=0.0, no_auto_scale=False, gamma=(1,1), chromatic_aberration=None, bad_pixels_path=None)

# Quant table at 100% (convert)
c_quant_100 = np.array([\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1],\
        [ 1,  1,  1,  1,  1,  1,  1,  1]])


# Quant table at 95% (convert)
c_quant_95 = np.array([\
        [ 2,  1,  1,  2,  2,  4,  5,  6],\
        [ 1,  1,  1,  2,  3,  6,  6,  6],\
        [ 1,  1,  2,  2,  4,  6,  7,  6],\
        [ 1,  2,  2,  3,  5,  9,  8,  6],\
        [ 2,  2,  4,  6,  7, 11, 10,  8],\
        [ 2,  4,  6,  6,  8, 10, 11,  9],\
        [ 5,  6,  8,  9, 10, 12, 12, 10],\
        [ 7,  9, 10, 10, 11, 10, 10, 10]])

# Quant table at 85% (convert)
c_quant_85 = np.array([\
     [ 5,  3,  3,  5,  7, 12, 15, 18],\
     [ 4,  4,  4,  6,  8, 17, 18, 17],\
     [ 4,  4,  5,  7, 12, 17, 21, 17],\
     [ 4,  5,  7,  9, 15, 26, 24, 19],\
     [ 5,  7, 11, 17, 20, 33, 31, 23],\
     [ 7, 11, 17, 19, 24, 31, 34, 28],\
     [15, 19, 23, 26, 31, 36, 36, 30],\
     [22, 28, 29, 29, 34, 30, 31, 30]])

# Quant table at 75% (convert)
c_quant_75 = np.array([\
        [ 8,  6,  5,  8, 12, 20, 26, 31],\
        [ 6,  6,  7, 10, 13, 29, 30, 28],\
        [ 7,  7,  8, 12, 20, 29, 35, 28],\
        [ 7,  9, 11, 15, 26, 44, 40, 31],\
        [ 9, 11, 19, 28, 34, 55, 52, 39],\
        [12, 18, 28, 32, 41, 52, 57, 46],\
        [25, 32, 39, 44, 52, 61, 60, 51],\
        [36, 46, 48, 49, 56, 50, 52, 50]])

def dct2(x):
    return dct(dct(x, norm='ortho').T, norm='ortho').T
    
def idct2(x):
    return idct(idct(x, norm='ortho').T, norm='ortho').T

def toYCbCr(_RGB, _name):
    skimage.io.imsave(os.getcwd()+'/temp_'+_name+'.tiff', _RGB)
    RGB = skimage.io.imread(os.getcwd()+'/temp_'+_name+'.tiff', plugin='tifffile')
    _R, _G, _B = RGB[ :, :,  0].astype('uint32'), RGB[ :, :, 1].astype('uint32'), RGB[ :, :, 2].astype('uint32')
    
    _Y = 0.299*_R+0.587*_G+0.114*_B
    _Cb = -0.168736*_R-0.331264*_G+0.5*_B
    _Cr = 0.5*_R-0.418688*_G-0.081312*_B
    
    _Y[_Y>2**16-1]=2**16-1
    _Cb[_Cb>2**16-1]=2**16-1
    _Cr[_Cr>2**16-1]=2**16-1
    
    os.remove(os.getcwd()+'/temp_'+_name+'.tiff')
    return np.dstack((_Y.astype(np.int32), _Cb.astype(np.int32), _Cr.astype(np.int32)))

# Compute DCT-Quantized coefficients from NON-quantized ones 
def compute_jpeg_from_dct(dct_im, c_quant):
    """
    Compute the jpeg representation from the DCT coefficients 
    """
    w, h = (dct_im.shape[0]//8)*8, (dct_im.shape[0]//8)*8
    jpeg_im = np.zeros((w, h))
    for bind_i in range(w//8):
        for bind_j in range(h//8):
            dct_bloc = dct_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]
            jpeg_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = np.round(dct_bloc/(c_quant*256))
    jpeg_im = jpeg_im.astype(np.int32)       
    return jpeg_im

# Compute the DCT quantized coefficients from a 16bits coded image
def compute_jpeg_domain(im_pix, c_quant):
    """
    Compress the image into JPEG (simulations)
    """
    w, h = (im_pix.shape[0]//8)*8, (dct_im.shape[0]//8)*8
    jpeg_im = np.zeros((w, h))
    for bind_i in range(w//8):
        for bind_j in range(h//8):
            im_bloc = im_pix[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]
            jpeg_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = np.round(dct2(im_bloc - 2**15-64)/(c_quant*256))
    jpeg_im = jpeg_im.astype(np.int16)

    return jpeg_im

def raw_to_dct(raw):
    rgb = raw[0].postprocess(params)
    ycbcr = toYCbCr(rgb, raw[1])
    dct_Y, dct_Cb, dct_Cr = compute_dct_domain(ycbcr[:, :, 0]),\
                            compute_dct_domain(ycbcr[:, :, 1]),\
                            compute_dct_domain(ycbcr[:, :, 2])
    return np.dstack((dct_Y, dct_Cb, dct_Cr))

def raw_to_ycbcr(raw):
    rgb = raw[0].postprocess(params)
    ycbcr = toYCbCr(rgb, raw[1])

    return ycbcr

def compute_dct_domain(im_pix):
    """
    Convert the image into DCT coefficients without performing quantization
    """
    w, h = im_pix.shape

    dct_im = np.zeros((w, h))
    for bind_i in range(w//8):
        for bind_j in range(h//8):
            im_bloc = im_pix[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8]
            dct_im[bind_i*8:(bind_i+1)*8,bind_j*8:(bind_j+1)*8] = dct2(im_bloc-2**15-64)
    return dct_im

def Crop(_arr, crop_size = (0, 0)):
    if(crop_size != (0, 0) and len(list(crop_size)) == 2):
        s_i, s_j =  _arr.shape[0]//crop_size[0],  _arr.shape[1]//crop_size[1]
        for i, j in itertools.product(range(s_i), range(s_j)):
            #np.save()
            print(i, j)
